import { useState } from "react";

const useInput = (validator) => {
  const [value, setValue] = useState("");
  const [hasFocused, setHasFocused] = useState(false);

  const isValid = validator(value);
  const hasError = !isValid && hasFocused;

  const valueChangeHandler = (event) => {
    setValue(event.target.value);
  };

  const inputBlurHandler = (event) => {
    setHasFocused(true);
  };

  const reset = () => {
    setValue("");
    setHasFocused(false);
  };

  return {
    value,
    isValid,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
  };
};

export default useInput;
